import requests
from urllib.parse import urlparse, urljoin
from bs4 import BeautifulSoup
import colorama
from time import sleep

#hash-function
import hashlib
 

# запускаем модуль colorama
colorama.init()

GREEN = colorama.Fore.GREEN
GRAY = colorama.Fore.LIGHTBLACK_EX
RESET = colorama.Fore.RESET
YELLOW = colorama.Fore.YELLOW
RED = colorama.Fore.RED

# инициализировать set's для внутренних и увиденных ссылок
internal_urls = set()
seen_urls = set()
error_urls = set()

def is_valid(url):
    """
    Проверка url
    """
    parsed = urlparse(url)
    return bool(parsed.netloc) and bool(parsed.scheme)


def get_all_website_links(url):
    # доменное имя URL без протокола
    domain_name = urlparse(url).netloc

    try:
        # Получаем "настоящий" адрес страницы и настоящий тип документа
        head_response = requests.head(url, allow_redirects=True)
    except Exception:
        # Если произошла ошибка при загрузке -- фейлимся
        print(f"{RED}[*] Ошибка при загрузке{RESET}")
        error_urls.add(url)
        return set()
    if head_response.status_code != 200: error_urls.add(url); return set()
    # Проверяем находимся ли мы ещё на нужном домене
    url = head_response.url
    if domain_name not in url:
        print(f"{GRAY}[*] Ушли с домена: {url}{RESET}")
        return set()

    # Проверяем не были ли мы на этом адресе
    if url in internal_urls:
        print(f"{GRAY}[*] Мы тут уже были: {url}{RESET}")
        return set()
    internal_urls.add(url)

    # Проверяем пришёл ли нам HTML документ
    if "text/html" not in head_response.headers["content-type"]:
        print(f"{GRAY}[*] Не HTML документ: {url}{RESET}")
        return set()
    
    # Ищем все ссылки на странице
    urls = set()
    soup = BeautifulSoup(requests.get(url, allow_redirects=True).content, "html.parser")
    for a_tag in soup.findAll("a"):
        href = a_tag.attrs.get("href")
        if href == "" or href is None:
            # пустой тег href
            continue
        # присоединяемся к URL, если он относительный (не абсолютная ссылка)
        href = urljoin(url, href)
        parsed_href = urlparse(href)
        # удалить параметры URL GET, фрагменты URL и т. д.
        href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path

        if not is_valid(href):
            # неверный URL
            continue
        if href in internal_urls or href in seen_urls:
            # уже в наборе или в очереди на проверку
            continue
        if domain_name not in href:
            # внешняя ссылка
            continue

        print(f"{GREEN}[*] Внутренняя ссылка: {href}{RESET}")
        urls.add(href)
        seen_urls.add(href)
    # sleep(10)
    return urls


def crawl(url):
    """
    Сканирует веб-страницу и извлекает все ссылки.
    Вы найдете все ссылки в глобальных переменных набора internal_urls.
    """
    print(f"{YELLOW}[*] Проверяем: {url}{RESET}")
    links = get_all_website_links(url)
    for link in links:
        crawl(link)


def hashFor(data):
    # Prepare the project id hash
    hashId = hashlib.md5()
    hashId.update(repr(data).encode('utf-8'))
    return hashId.hexdigest()


if __name__ == "__main__":
    url = 'https://www.nstu.ru/'
   
    crawl(url)

    print("[+] Total Internal links:", len(internal_urls))

    domain_name = urlparse(url).netloc
    
    # сохранить внутренние ссылки в файле
    with open(f"{domain_name}_internal_links.txt", "w") as f:
        for internal_link in internal_urls:
            print(hashFor(internal_link) + ':', internal_link.strip(), file=f)
            
    with open(f"{domain_name}_error_urls.txt", "w") as f:
        for error_urls in error_urls:
            print(error_urls.strip(), file=f)        
    
